#pragma once
#include "Component.h"
class FinishLogic : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(FinishLogic, Component)

private:
	GameObject* player = nullptr;
	std::string currentLevelPath = "";
	std::string nextLevelPath = "";

	

protected:
	void initialize() override;

public:
	FinishLogic() = default;
	~FinishLogic();
	void update(float deltaTime) override;
	void load(json::JSON& node) override;
	void onTriggerEnter(const Collision* const collisionData) override;
	void onCollisionEnter(const Collision* const collisionData) override;
	void setEnabled(bool _enabled) override;

};

