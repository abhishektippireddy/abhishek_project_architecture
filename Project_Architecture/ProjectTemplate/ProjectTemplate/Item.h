#pragma once
#include "Component.h"
class Item : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Item, Component)

private:
	GameObject* player = nullptr;

protected:
	void initialize() override;

public:
	Item() = default;
	~Item();
	void update(float deltaTime) override;
	void load(json::JSON& node) override;
	void onTriggerEnter(const Collision* const collisionData) override;
	void onCollisionEnter(const Collision* const collisionData) override;
	void setEnabled(bool _enabled) override;

};

