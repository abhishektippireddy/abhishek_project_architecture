#include "GameCore.h"
#include "Enemy.h"
#include "ICollidable.h"

IMPLEMENT_DYNAMIC_CLASS(Enemy)

void Enemy::initialize()
{
	if (!isEnabled())
	{
		return;
	}
	Component::initialize();

	std::string playerstring = "Player";
	player = GameObjectManager::instance().getGameObjectWithComponent(playerstring);
	
	
}

Enemy::~Enemy()
{
}

void Enemy::update(float deltaTime)
{
	if (player != nullptr)
	{
		playerPosition = player->getTransform()->getPosition();
		enemyPosition = getGameObject()->getTransform()->getPosition();
		moveDirection = playerPosition - enemyPosition;

		float normal = sqrt((moveDirection.x * moveDirection.x) + ((moveDirection.y * moveDirection.y)));

		moveDirection.x /= normal;
		moveDirection.y /= normal;

		getGameObject()->getTransform()->translate(moveDirection * deltaTime * enemyMoveSpeed);
	}
	
}

void Enemy::load(json::JSON& node)
{

	if (node.hasKey("enemyMoveSpeed"))
	{
		enemyMoveSpeed = node["enemyMoveSpeed"].ToFloat();
	}

}

void Enemy::onTriggerEnter(const Collision* const collisionData)
{
}

void Enemy::onCollisionEnter(const Collision* const collisionData)
{
	int otherColliderIx = 1;
	if (collisionData->colliders[otherColliderIx]->getGameObject() == getGameObject())
	{
		otherColliderIx = 0;
	}
	if (collisionData->colliders[otherColliderIx]->getGameObject() != player)
	{
		if (collisionData->colliders[otherColliderIx]->getGameObject()->getComponent("Projectile") != nullptr)
		{
			GameObjectManager::instance().removeGameObject(getGameObject());
		}
	}
}

void Enemy::setEnabled(bool _enabled)
{
	enabled = _enabled;
	if (enabled && getGameObject()->isEnabled() && !initialized)
	{
		initialize();
	}
}
