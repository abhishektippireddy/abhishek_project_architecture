#include "GameCore.h"
#include "Player.h"
#include "ProjectEngine.h"
#include "ICollidable.h"
#include "PrefabAsset.h"

IMPLEMENT_DYNAMIC_CLASS(Player)

void Player::initialize()
{
	if (!isEnabled())
	{
		return;
	}
	Component::initialize();
	ProjectEngine::instance().setPlayer(this);
}

Player::~Player()
{
	ProjectEngine::instance().setPlayer(nullptr);
}

void Player::update(float deltaTime)
{
	if (!getGameObject()->isEnabled() || !enabled)
	{
		return;
	}
	if (win)
	{
		return;
	}

	sf::Vector2f playerPosition = getGameObject()->getTransform()->getPosition();
	sf::Vector2f moveOffset(0, 0);

	if (InputManager::instance().getKeyState(sf::Keyboard::Tilde) == InputManager::PushState::Up)
	{
		ProjectEngine::instance().pauseMenu();
	}

	if (InputManager::instance().getKeyState(sf::Keyboard::Down) == InputManager::PushState::Held)
	{
		moveOffset.y += moveSpeed * deltaTime;
	}
	if (InputManager::instance().getKeyState(sf::Keyboard::Up) == InputManager::PushState::Held)
	{
		moveOffset.y -= moveSpeed * deltaTime;
	}
	if (InputManager::instance().getKeyState(sf::Keyboard::Left) == InputManager::PushState::Held)
	{
		moveOffset.x -= moveSpeed * deltaTime;
	}
	if (InputManager::instance().getKeyState(sf::Keyboard::Right) == InputManager::PushState::Held)
	{
		moveOffset.x += moveSpeed * deltaTime;
	}
	if (InputManager::instance().getMouseButtonState(sf::Mouse::Button::Left) == InputManager::PushState::Up)
	{
		/*std::cout << "Mouse Button clicked" << std::endl;
		sf::Vector2f mouseposition = InputManager::instance().getMousePosition();
		std::cout << mouseposition.x  << std::endl;
		std::cout << mouseposition.y << std::endl;*/

		PrefabAsset* projectilePrefab = (PrefabAsset*)AssetManager::instance().GetAssetByGUID(projectileGuid);

		if (projectilePrefab != nullptr)
		{
			GameObject* projectileGo = GameObjectManager::instance().instantiatePrefab(projectilePrefab->getID());
			projectileGo->getTransform()->setPosition(playerPosition);
		}
	}

	getGameObject()->getTransform()->translate(moveOffset);
	
}

void Player::load(json::JSON& node)
{
	Component::load(node);
	if (node.hasKey("moveSpeed"))
	{
		moveSpeed = node["moveSpeed"].ToFloat();
	}

	if (node.hasKey("projectileGuid"))
	{
		projectileGuid = node["projectileGuid"].ToString();
	}

}

void Player::setEnabled(bool _enabled)
{
	enabled = _enabled;
	if (enabled && getGameObject()->isEnabled() && !initialized)
	{
		initialize();
	}
}


void Player::onTriggerEnter(const Collision* const collisionData)
{
	int otherColliderIx = 1;
	if (collisionData->colliders[otherColliderIx]->getGameObject() == getGameObject())
	{
		otherColliderIx = 0;
	}

	if (collisionData->colliders[otherColliderIx]->getGameObject()->getComponent("Text") != nullptr)
	{
		collisionData->colliders[otherColliderIx]->getGameObject()->getComponent("Text")->setEnabled(true);
		win = true;
	}	
}
