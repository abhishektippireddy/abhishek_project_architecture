///-------------------------------------------------------------------------------------------------
// file: ProjectEngine.h
//
// author: Huzaifa Saboowala
// date: 12/15/2019
//
// summary:	The Project Engine
///-------------------------------------------------------------------------------------------------
#ifndef _PROJECTENGINE_H_
#define _PROJECTENGINE_H_
#pragma once

#include "ISystem.h"
class Player;
class ProjectEngine final : public ISystem
{
	Player* playerScript = nullptr;
	std::string levelFile = "../Assets/Levels/level1.json";
	bool loaded = false;
	sf::RenderWindow mainMenuWindow;
	sf::RenderWindow pauseWindow;

public:
    void initialize() override;
    void update(float deltaTime)override;
	void setPlayer(Player* _player) { playerScript = _player; }
	void mainMenu();
	void pauseMenu();
    DECLARE_SINGLETON(ProjectEngine)
};

#endif