#pragma once
#include "Component.h"
class Enemy : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Enemy, Component)

private:
	GameObject* player = nullptr;
	sf::Vector2f enemyPosition;
	sf::Vector2f playerPosition;
	sf::Vector2f moveDirection;

protected:
	void initialize() override;

public:
	float enemyMoveSpeed = 100.0f;	//Default move speed if its not assigned in the level file
	Enemy() = default;
	~Enemy();
	void update(float deltaTime) override;
	void load(json::JSON& node) override;
	void onTriggerEnter(const Collision* const collisionData) override;
	void onCollisionEnter(const Collision* const collisionData) override;
	void setEnabled(bool _enabled) override;

};

