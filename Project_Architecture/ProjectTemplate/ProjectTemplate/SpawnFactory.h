#pragma once
#include "Component.h"
class SpawnFactory : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(SpawnFactory, Component)

private:
	std::string enemyGuid;
	std::string itemGuid;
	float enemySpawnTime = 0.0f;
	float timer = 0.0f;

protected:
	void initialize() override;

public:
	SpawnFactory() = default;
	~SpawnFactory();
	void update(float deltaTime) override;
	void load(json::JSON& node) override;
	void onTriggerEnter(const Collision* const collisionData) override;
	void onCollisionEnter(const Collision* const collisionData) override;
	void setEnabled(bool _enabled) override;

};

