#include "GameCore.h"
#include "FinishLogic.h"

IMPLEMENT_DYNAMIC_CLASS(FinishLogic)

void FinishLogic::initialize()
{
	if (!isEnabled())
	{
		return;
	}
	Component::initialize();

	std::string playerstring = "Player";
	player = GameObjectManager::instance().getGameObjectWithComponent(playerstring);
}

FinishLogic::~FinishLogic()
{
}

void FinishLogic::update(float deltaTime)
{
}

void FinishLogic::load(json::JSON& node)
{
	if (node.hasKey("currentLevelPath"))
	{
		currentLevelPath = node["currentLevelPath"].ToString();
	}

	if (node.hasKey("nextLevelPath"))
	{
		nextLevelPath = node["nextLevelPath"].ToString();
	}

}

void FinishLogic::onTriggerEnter(const Collision* const collisionData)
{

	if (currentLevelPath != "" && nextLevelPath != "")
	{
		FileSystem::instance().unload(currentLevelPath);
		FileSystem::instance().load(nextLevelPath, true);
	}
}

void FinishLogic::onCollisionEnter(const Collision* const collisionData)
{
}

void FinishLogic::setEnabled(bool _enabled)
{
	enabled = _enabled;
	if (enabled && getGameObject()->isEnabled() && !initialized)
	{
		initialize();
	}
}
