#include "GameCore.h"
#include "Projectile.h"
#include "ICollidable.h"

IMPLEMENT_DYNAMIC_CLASS(Projectile)

void Projectile::initialize()
{
	if (!isEnabled())
	{
		return;
	}
	Component::initialize();

	//std::cout << "Projectile created" << std::endl;
	std::string playerstring = "Player";
	mousePosition =	InputManager::instance().getMousePosition();
	player = GameObjectManager::instance().getGameObjectWithComponent(playerstring);
	if (player != nullptr)
	{
		projectilePosition = player->getTransform()->getPosition();
		fireDirection = mousePosition - projectilePosition;

		float normal = sqrt((fireDirection.x * fireDirection.x) + ((fireDirection.y * fireDirection.y)));

		fireDirection.x /= normal;
		fireDirection.y /= normal;

	}
}

Projectile::~Projectile()
{
	player = nullptr;
	//std::cout << "Projectile destroyed" << std::endl;
}

void Projectile::update(float deltaTime)
{
		/*std::cout << "Mouse Button clicked" << std::endl;
		sf::Vector2f mouseposition = InputManager::instance().getMousePosition();
		std::cout << mouseposition.x  << std::endl;
		std::cout << mouseposition.y << std::endl;*/

	getGameObject()->getTransform()->translate(fireDirection * deltaTime * projectileSpeed);
}

void Projectile::load(json::JSON& node)
{
	Component::load(node);
	if (node.hasKey("projectileSpeed"))
	{
		projectileSpeed = node["projectileSpeed"].ToFloat();
	}
}

void Projectile::onTriggerEnter(const Collision* const collisionData)
{
	//std::cout << "Projectile Triggered" << std::endl;
}

void Projectile::onCollisionEnter(const Collision* const collisionData)
{
	int otherColliderIx = 1;
	if (collisionData->colliders[otherColliderIx]->getGameObject() == getGameObject())
	{
		otherColliderIx = 0;
	}

	if (collisionData->colliders[otherColliderIx]->getGameObject() != player)
	{
		if (collisionData->colliders[otherColliderIx]->getGameObject()->getComponent("Projectile") == nullptr) //Check whether its not colliding with some other projectile
		{
			GameObjectManager::instance().removeGameObject(getGameObject());
		}
	}
}

void Projectile::setEnabled(bool _enabled)
{
	enabled = _enabled;
	if (enabled && getGameObject()->isEnabled() && !initialized)
	{
		initialize();
	}
}
